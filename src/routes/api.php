<?php

use App\Http\Controllers\ReviewController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


Route::prefix('/v1')->group(function () {
    Route::prefix('/review')->name('review.')->group(function () {
        Route::post('/', [ReviewController::class, 'store'])->name('store');
    });
});
