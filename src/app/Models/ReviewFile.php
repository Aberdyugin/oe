<?php

namespace App\Models;

use App\Actions\ReviewFilesAction;
use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReviewFile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'path',
        'review_id',
    ];

    public function review()
    {
        return $this->belongsTo(Review::class);
    }
}
