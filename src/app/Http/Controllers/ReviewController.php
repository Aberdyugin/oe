<?php

namespace App\Http\Controllers;

use App\Actions\ReviewFilesAction;
use App\Http\Requests\ReviewRequest;
use App\Models\Review;
use App\Models\ReviewFile;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ReviewController extends Controller
{
    use AuthorizesRequests;


    /**
     * @param ReviewRequest $request
     * @return Review
     *
     * @OA\Post(
     *     path="/api/v1/review",
     *     tags={"Review"},
     *
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *              @OA\Property (property="name", type="string", example="Kon' v palto"),
     *              @OA\Property (property="email", type="string", example="kon@v.palto"),
     *              @OA\Property (property="phone", type="string", example="+987654321"),
     *              @OA\Property (property="city", type="string", example="HorseCity"),
     *              @OA\Property (property="subject", type="string", example="Where is a oats"),
     *              @OA\Property (property="body", type="string", example="i'm huhgry")
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="Ok",
     *         @OA\JsonContent(
     *             @OA\Property (property="id", type="integer", example="123654"),
     *             @OA\Property (property="name", type="string", example="Alex"),
     *             @OA\Property (property="email", type="string", example="Alex@mail.no"),
     *             @OA\Property (property="phone", type="string", example="+789654123"),
     *             @OA\Property (property="city", type="string", example="Hometoen"),
     *             @OA\Property (property="subject", type="string", example="Problem"),
     *             @OA\Property (property="body", type="string", example="I have a problem! But, your APP is Beautiful!!!")
     *
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     )
     * ).
     *
     */
    public function store(ReviewRequest $request)
    {
        $review = Review::create($request->validated());
        if ($request->file('file')) {
            $fileName = (new ReviewFilesAction())($request->file('file'));

            $review->file()->create([
                'path' => $fileName,
            ]);
        }
        return $review;
    }

}
