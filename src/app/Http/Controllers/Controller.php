<?php

namespace App\Http\Controllers;


/**
 * @OA\Info(title="OE test API", version="0.0.1")
 * @OA\PathItem (
 *      path="/api/v1"
 *  )
 */
abstract class Controller
{
    //
}
