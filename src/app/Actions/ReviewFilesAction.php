<?php

namespace App\Actions;

use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ReviewFilesAction
{
    public function __construct()
    {
    }

    public function __invoke($file)
    {
        $prefix = Str::random(10) . '_';
        $fName = $prefix . $file->getClientOriginalName();
        Storage::disk('local')->put($fName, file_get_contents($file->getRealPath()));
        return $fName;
    }
}
